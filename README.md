# NRF52 Dongle

I'm using [OpenThread](https://openthread.io) to control my home automation
devices. They are built around a [nRF52840
SoC](https://www.nordicsemi.com/products/nrf52840), provided by a [Holyiot
18010 module](http://www.holyiot.com/eacp_view.asp?id=278) with built-in
antenna. For the OpenThread gateway, I've been using the [nRF52840
Dongle](https://www.nordicsemi.com/Products/Development-hardware/nrf52840-dongle)
from [Nordic Semiconductor](https://www.nordicsemi.com), running as a [Radio
Co-Processor (RCP)](https://openthread.io/platforms/co-processor). While it's
good for prototyping, I'm not completely happy with it for this purpose, as it
doesn't fit securely in a USB port, and has a weak antenna.

That's why I decided to make my own nRF52840 based dongle, with an SMA
connector for an external antenna.


## Photos

[![NRF52 Dongle](images/nrf52-dongle-small.jpeg)](images/nrf52-dongle.jpeg)

[![NRF52 Dongle with enclosure](images/nrf52-dongle-enclosure-small.jpeg)](images/nrf52-dongle-enclosure.jpeg)


## Hardware

Since I'm not experienced in designing RF PCBs, I decided to use an existing
module with a nRF52840 SoC. Searching on the internet, I found the [NINA-B301
module](https://www.u-blox.com/en/product/nina-b30-series-open-cpu-0) from
[u-blox](https://www.u-blox.com), which is based on the nRF52840 SoC and has a
pin for the RF output that can be connected to an SMA connector.

The board was designed using [KiCad](https://www.kicad.org/) 6.0. The
[schematic](images/schematic.png) is simple because most the work is done by
the NINA-B301 module, which includes both 32 MHz and 32.768 kHz crystals. In
addition there is an [ARM
SWD](https://developer.arm.com/documentation/ihi0031/a/The-Serial-Wire-Debug-Port--SW-DP-/Introduction-to-the-ARM-Serial-Wire-Debug--SWD--protocol)
connector for programming, a voltage regulator providing 3.3V from the 5V USB,
and a USB connector with the corresponding ESD protection and power filtering.

I used the same pin for the LED as the green LED on the Nordic nRF52840 dongle
to be able to reuse the existing firmares. However, I chose not to include a
RGB LED or a reset switch.

The PCB is a two-layer design and can fit into a [Hammond
1551USB2](https://www.hammfg.com/electronics/small-case/plastic/1551usb)
enclosure that I'm using as a standard. The RF trace between the NINA-B301
module and the SMA connector is kept as short as possible, with 50 ohm
impedance matching making it wide given the 2-layer design. On the other hand,
I didn't used differential impedance matching for the USB traces. This isn't
really needed for full-speed devices and when the traces are kept short, and
it's pretty tough to do a 2-layer PCB. I just made sure the two traces were
the same length.

All the components from the [bill of material](BOM.txt) should be easily
available from many distributors. Soldering of the NINA-B301 module requires a
hot air gun, but the LGA style pads are relatively large, so it's relatively
easy to do. I personally didn't use a stencil.


## Software

I haven't developped any a custom software for this dongle. Instead, I am using
the [ot-nrf528xx](https://github.com/openthread/ot-nrf528xx) repository to
build an RCP firmware with a few commands:

```
  ./script/build nrf52840 USB_trans
  cd ./build/bin
  arm-none-eabi-objcopy -O ihex ot-rcp ot-rcp.hex
```

Then the resulting HEX file can be flashed with:

```
  pyocd flash -e chip -t nrf52 ot-rcp.hex
```

The nRF52840 SoC is quite powerful, so it can also be used as a Matter or BLE
dongle, or for other purposes as well.


## License

The contents of this repository is released under the [Creative Commons
Attribution-ShareAlike 4.0 International License (CC BY-SA 4.0)](LICENSE).
